package com.lwq.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;



@Getter
@Setter
//贴有该注解的字符会被映射为对应的集合名字
@Document(collection = "fs.files")
public class files {
    /**
     * 主键id
     */
    private String _id;

    /**
     * 文件名称
     */
    private String filename;

    /**
     * 长度
     */
    private Integer length;

    /**
     *大小
     */
    private Integer chunkSize;

    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date uploadDate;

    /**
     *类型
     */
    private Object metadata;
    //List<metadata> metadata;
}
