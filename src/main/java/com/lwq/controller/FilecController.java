package com.lwq.controller;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.*;
import com.lwq.entity.files;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.support.QuerydslRepositorySupport;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.nio.ch.IOUtil;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;

/**
 * 文件上传下载
 */
@Controller
@RequestMapping("/file")
public class FilecController {

    //获得springboot 提供得mongodb的 gridfs 对象
    @Autowired
    private GridFsTemplate gridFsTemplate;
    //MONGODB 用于增删查改
    @Autowired
    private MongoTemplate mongoTemplate;


    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    public Object uploadFile(@RequestParam("file") MultipartFile file) throws IOException, ServletException {
        //文件名称
        String originalFilename = file.getOriginalFilename();
        if (originalFilename == null) {
            return "请上传文件";
        }
        //获得文件输入流
        InputStream inputStream = file.getInputStream();
        //获得文件类型
        String contentType = file.getContentType();
        //将文件存储到mongodb 中,mongodb将会返回这个文件得具体信息
        ObjectId gridFSFile = gridFsTemplate.store(inputStream, originalFilename, contentType);
        System.out.println(gridFSFile);
        return gridFSFile.toString();

    }

    /**
     * 文件下载 如果是图片可以直接通过<IMG SRC = '这个方法的访问路径加上文件的id'
     * 下载直接 get 请求调用
     * 回显 需要特殊方法
     * 例如图片回显 <img src="http://127.0.0.1:8081/download?file=${ObjectId}"></img>
     * 当中的ObjectId是文件对应的图片id 路径就是访问的本地下载的页面
     *
     * @return
     */
    @RequestMapping("/download")
    public void download(String objectid, HttpServletResponse response) throws IOException {
        System.out.println("---------下传文件");
        //根据id 查询数据
        Query id = Query.query(Criteria.where("_id").is(objectid));
        GridFSFile fsFiles = gridFsTemplate.findOne(id);
        if (fsFiles == null) {
            System.out.println("没用获取到文件");
        }
        //文件名称
        String s = fsFiles.getFilename();
        //文件类型
        Document metadata = fsFiles.getMetadata();
        if (metadata == null) {
            System.out.println("\"获取文件为空\";");
        }
        String contentType = fsFiles.getMetadata().get("_contentType").toString();
        System.out.println("文件类型：" + contentType);
        //通知下载
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(s, "UTF-8") + "\"");
        //mongdb文件操作 因为他是分片存的
        GridFSBucket gridFSBucket = GridFSBuckets.create(mongoTemplate.getDb());
        GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(fsFiles.getObjectId());
        //获取mongodb的资源
        GridFsResource gridFsResource = new GridFsResource(fsFiles, gridFSDownloadStream);
        //文件输入流
        InputStream inputStream = gridFsResource.getInputStream();
        //文件输出流
        ServletOutputStream outputStream = response.getOutputStream();
        //拷贝流
        IOUtils.copy(inputStream, outputStream);
        //关流
        outputStream.flush();
        outputStream.close();
        inputStream.close();
        System.out.println("-------下载完成------------");
    }

    /**
     * 根据文件id 删除mongodb的文件
     *
     * @return
     */
    @RequestMapping("/deleteFile")
    public String deleteFile(String objectId) throws Exception {
        System.out.println("删除文件");
        Query id = Query.query(Criteria.where("_id").is(objectId));
        GridFSFile one = gridFsTemplate.findOne(id);
        if (one == null) {
            return "文件不存在";
        }
        //操作mongodb 分片文档
        GridFSBucket gridFSBucket = GridFSBuckets.create(mongoTemplate.getDb());
        //删除分片
        gridFSBucket.delete(new ObjectId(objectId));
        return "删除成功";
    }


    /**
     * 查询多条数据 属于分级查询 limit 不指定就查询所有
     * skip 表示跳过行数  不指定就跳过
     *
     * @param page
     * @param pageSize
     * @return
     */
    @ResponseBody
    @GetMapping("/getPage")
    public PageImpl<files> getPage(int page, int pageSize) {
        System.out.println("查询多条数据:属于分级查询");
        //mongodb 构造器
        Query query = new Query();
        //每页五个
        Pageable pageable = PageRequest.of(page, pageSize);
        query.with(pageable);
        //按sql排序
        query.with(Sort.by(Sort.Direction.DESC, "metadDate"));
        //mongodb 查询全部数据
        Query queryCount = new Query();
        //按sql排序
        queryCount.with(Sort.by(Sort.Direction.DESC, "_id"));
        //查询总数
        Long count = mongoTemplate.count(queryCount, files.class);
        List<files> r = mongoTemplate.find(query, files.class);
        return (PageImpl<files>) PageableExecutionUtils.getPage(r, pageable, () -> count);
    }

    /**
     * pdf 添加水印  右下角单个水印
     *
     * @param objectid 下载的id
     * @param response 请求
     * @throws IOException
     */
    @RequestMapping("/download5")
    public void downloadWatermark5(String objectid, HttpServletResponse response) throws IOException, DocumentException, DocumentException {
        System.out.println("---------下传文件");
        //根据id 查询数据
        Query id = Query.query(Criteria.where("_id").is(objectid));
        GridFSFile fsFiles = gridFsTemplate.findOne(id);
        if (fsFiles == null) {
            System.out.println("没用获取到文件");
        }
        //文件名称
        String s = fsFiles.getFilename();
        //文件类型
        Document metadata = fsFiles.getMetadata();
        if (metadata == null) {
            System.out.println("\"获取文件为空\";");
        }
        String contentType = fsFiles.getMetadata().get("_contentType").toString();
        System.out.println("文件类型：" + contentType);
        //通知下载
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(s, "UTF-8") + "\"");
        GridFSBucket gridFSBucket = GridFSBuckets.create(mongoTemplate.getDb());
        GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(fsFiles.getObjectId());
        //获取mongodb的资源
        GridFsResource gridFsResource = new GridFsResource(fsFiles, gridFSDownloadStream);

        //pdf 下载
        //pdf 的输入文件
        PdfReader reader = new PdfReader(gridFsResource.getInputStream());
        //输入和输出
        PdfStamper stamper = new PdfStamper(reader,response.getOutputStream());
        //获取总页数+1，下面从1开始遍历
        int total = reader.getNumberOfPages()+1;
        // 使用calsspath 下面字体库
        BaseFont base =null;
        //字体库 具体可以百度
        base = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);

// 设置水印透明度
        PdfGState gs = new PdfGState();
        gs.setFillOpacity(0.4f);
        gs.setStrokeOpacity(0.4f);

        PdfContentByte content = null;
        for (int i = 1; i < total; i++) {
            // 在内容上方加水印
            content = stamper.getOverContent(i);
            // 在内容下方加水印
            // content = stamper.getUnderContent(i);
            content.saveState();
            content.setGState(gs);

            // 设置字体和字体大小
            content.beginText();
            content.setFontAndSize(base, 20);

            // 设置字体样式
            float ta = 1F, tb = 0F, tc = 0F, td = 1F, tx = 0F, ty = 0F;
            // 设置加粗(加粗)
            ta += 0.25F;
            td += 0.05F;
            ty -= 0.2F;
            // 设置倾斜(倾斜程序自己改)
            tc += 0.8F;
            content.setTextMatrix(ta, tb, tc, td, tx, ty);

            // 设置相对于左下角位置(向右为x，向上为y)
            content.moveText(400F, 5F);
            // 显示text
            content.showText("赖伟强");

            content.endText();
            content.stroke();
            content.restoreState();
        }

        // 关流
        stamper.close();
        reader.close();

    }

    /**
     * pdf 添加水印  铺满水印
     *
     * @param objectid 下载的id
     * @param response 请求
     * @throws IOException
     */
    @RequestMapping("/download4")
    public void downloadWatermark4(String objectid, HttpServletResponse response) throws IOException, DocumentException, DocumentException {
        System.out.println("---------下传文件");
        //根据id 查询数据
        Query id = Query.query(Criteria.where("_id").is(objectid));
        GridFSFile fsFiles = gridFsTemplate.findOne(id);
        if (fsFiles == null) {
            System.out.println("没用获取到文件");
        }
        //文件名称
        String s = fsFiles.getFilename();
        //文件类型
        Document metadata = fsFiles.getMetadata();
        if (metadata == null) {
            System.out.println("\"获取文件为空\";");
        }
        String contentType = fsFiles.getMetadata().get("_contentType").toString();
        System.out.println("文件类型：" + contentType);
        //通知下载
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(s, "UTF-8") + "\"");
        GridFSBucket gridFSBucket = GridFSBuckets.create(mongoTemplate.getDb());
        GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(fsFiles.getObjectId());
        //获取mongodb的资源
        GridFsResource gridFsResource = new GridFsResource(fsFiles, gridFSDownloadStream);

        //pdf 下载
        //pdf 的输入文件
        PdfReader reader = new PdfReader(gridFsResource.getInputStream());
        //输入和输出
        PdfStamper stamper = new PdfStamper(reader,response.getOutputStream());
        //获取总页数+1，下面从1开始遍历
        int total = reader.getNumberOfPages()+1;
        // 使用calsspath 下面字体库
        BaseFont base =null;
        //字体库 具体可以百度
        base = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",BaseFont.NOT_EMBEDDED);

        //间隔
        int interval = -5;
        // 获取水印文字的高度和宽度
        int texh = 0;
        int textw =0;
        JLabel label =new JLabel();
        //水印名称
        label.setText("赖伟强");
        //字体的高度和宽度
        FontMetrics metrics = label.getFontMetrics(label.getFont());
        texh = metrics.getHeight();
        textw = metrics.stringWidth(label.getText());
        System.out.println("textH: " + texh);
        System.out.println("textW: " + textw);

        // 设置水印透明度
        PdfGState gs = new PdfGState();
        gs.setFillOpacity(0.4f);
        gs.setStrokeOpacity(0.4f);

        com.itextpdf.text.Rectangle pageSizeWithRotation = null;
        PdfContentByte content = null;
        for (int i = 1; i < total; i++) {
            // 在内容上方加水印
            //content = stamper.getOverContent(i);
            //在内容下下方加水印
            content = stamper.getUnderContent(i);
            System.out.println(gs);
            content.setGState(gs);
            content.saveState();

            //设置字体和字体大小
            content.beginText();
            content.setFontAndSize(base,20);

            //获取每一页的高度 宽度
            pageSizeWithRotation = reader.getPageSizeWithRotation(i);
            float pageHeight = pageSizeWithRotation.getHeight();
            float pageWidth = pageSizeWithRotation.getWidth();

            // 铺满 根据纸张大小多次添加， 水印文字成30度角倾斜
          for (int height = interval + texh; height < pageHeight; height = height + textw * 3) {
               for (int width = interval +textw; width < pageWidth + textw; width = width + textw * 2) {
                  content.showTextAligned(Element.ALIGN_LEFT, "赖伟强", width - textw, height - texh, 30);
              }
            }
            content.endText();

        }
        // 关流
        stamper.close();
        reader.close();

    }


    /**
     * 图片以画布得方式添加水印  单个水印
     *
     * @param objectid
     * @param response
     * @throws IOException
     */
    @RequestMapping("/download2")
    public void downloadWatermark(String objectid, HttpServletResponse response) throws IOException {
        System.out.println("---------下传文件");
        //根据id 查询数据
        Query id = Query.query(Criteria.where("_id").is(objectid));
        GridFSFile fsFiles = gridFsTemplate.findOne(id);
        if (fsFiles == null) {
            System.out.println("没用获取到文件");
        }
        //文件名称
        String s = fsFiles.getFilename();
        //文件类型
        Document metadata = fsFiles.getMetadata();
        if (metadata == null) {
            System.out.println("\"获取文件为空\";");
        }
        String contentType = fsFiles.getMetadata().get("_contentType").toString();
        System.out.println("文件类型：" + contentType);
        //通知下载
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(s, "UTF-8") + "\"");
        GridFSBucket gridFSBucket = GridFSBuckets.create(mongoTemplate.getDb());
        GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(fsFiles.getObjectId());
        //获取mongodb的资源
        GridFsResource gridFsResource = new GridFsResource(fsFiles, gridFSDownloadStream);

        Color color = new Color(255, 200, 0, 118);   // 水印颜色
        Font font = new Font("微软雅黑", Font.ITALIC, 45);  //水印字体
        String waterMarkContent = "赖伟强";   //水印内容
        BufferedImage buImage = ImageIO.read(gridFsResource.getInputStream());
        int width = buImage.getWidth(); //图片宽
        int height = buImage.getHeight(); //图片高

        //添加水印
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
        Graphics2D g = bufferedImage.createGraphics();
        g.drawImage(buImage, 0, 0, width, height, null);
        g.setColor(color); //水印颜色
        g.setFont(font); //水印字体

        int x = width - 2 * getWatermarkLength(waterMarkContent, g);  //这是一个计算水印位置的函数，可以根据需求添加
        int y = height - 1 * getWatermarkLength(waterMarkContent, g);
        //g.drawString(waterMarkContent, 400, 300); //水印位置
        //水印位置 位置在右下角
        g.drawString(waterMarkContent, x, y);
        g.dispose(); //释放资源

        ServletOutputStream outputStream = response.getOutputStream();
        ImageIO.write(bufferedImage, "jpg", outputStream);
        System.out.println("添加水印完成");

        outputStream.flush();
        outputStream.close();
    }

    /**
     * 图片 以画布得方式添加水印  铺满的水印
     *
     * @param objectid
     * @param response
     * @throws IOException
     */
    @RequestMapping("/download3")
    public void downloadWatermark3(String objectid, HttpServletResponse response) throws IOException {
        System.out.println("---------下传文件");
        //根据id 查询数据
        Query id = Query.query(Criteria.where("_id").is(objectid));
        GridFSFile fsFiles = gridFsTemplate.findOne(id);
        if (fsFiles == null) {
            System.out.println("没用获取到文件");
        }
        //文件名称
        String s = fsFiles.getFilename();
        //文件类型
        Document metadata = fsFiles.getMetadata();
        if (metadata == null) {
            System.out.println("\"获取文件为空\";");
        }
        String contentType = fsFiles.getMetadata().get("_contentType").toString();
        System.out.println("文件类型：" + contentType);
        //通知下载
        response.setContentType(contentType);
        response.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode(s, "UTF-8") + "\"");
        GridFSBucket gridFSBucket = GridFSBuckets.create(mongoTemplate.getDb());
        GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(fsFiles.getObjectId());
        //获取mongodb的资源
        GridFsResource gridFsResource = new GridFsResource(fsFiles, gridFSDownloadStream);

        // 设置水印文字透明度
        // 水印透明度
        float alpha = 0.5f;
        // 水印文字大小
        int FONT_SIZE = 18;
        // 水印文字字体
        Font font = new Font("宋体", Font.PLAIN, FONT_SIZE);
        // 水印文字颜色
        Color color = Color.gray;
        // 水印之间的间隔
        int XMOVE = 80;
        // 水印之间的间隔
        int YMOVE = 80;

        String waterMarkContent = "赖伟强";   //水印内容
        BufferedImage buImage = ImageIO.read(gridFsResource.getInputStream());
        //图片宽
        int width = buImage.getWidth();
        //图片高
        int height = buImage.getHeight();
        BufferedImage buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 得到画笔对象
        Graphics2D g = buffImg.createGraphics();
        // 设置对线段的锯齿状边缘处理
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(buImage.getScaledInstance(width, height, Image.SCALE_SMOOTH),
                0, 0, null);
        // 设置水印旋转角度
        g.rotate(Math.toRadians(-40), (double) buffImg.getWidth() / 2, (double) buffImg.getHeight() / 2);

        // 设置水印文字颜色
        g.setColor(color);
        // 设置水印文字Font
        g.setFont(font);

        int x = -width / 2;
        int y = -height / 2;
        int markWidth = FONT_SIZE * getTextLength(waterMarkContent);// 字体长度
        int markHeight = FONT_SIZE;// 字体高度

        // 循环添加水印
        while (x < width * 1.5) {
            y = -height / 2;
            while (y < height * 1.5) {
                g.drawString(waterMarkContent, x, y);

                y += markHeight + YMOVE;
            }
            x += markWidth + XMOVE;
        }
        // 释放资源
        g.dispose();
        // 生成图片
        ServletOutputStream outputStream = response.getOutputStream();
        ImageIO.write(buffImg, "JPG", outputStream);
        System.out.println("添加水印文字成功!");

        outputStream.flush();
        outputStream.close();
    }

    /**
     * 获取文本长度。汉字为1:1，英文和数字为2:1
     */
    private static int getTextLength(String text) {
        int length = text.length();
        for (int i = 0; i < text.length(); i++) {
            String s = String.valueOf(text.charAt(i));
            if (s.getBytes().length > 1) {
                length++;
            }
        }
        length = length % 2 == 0 ? length / 2 : length / 2 + 1;
        return length;
    }


    /**
     * 计算文字的高度和宽度
     *
     * @param waterMarkContent 字体
     * @param g                画布
     * @return
     */
    public static int getWatermarkLength(String waterMarkContent, Graphics2D g) {
        return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(), 0, waterMarkContent.length());
    }

    /**
     * 测试
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Color color = new Color(255, 200, 0, 118);   // 水印颜色
        Font font = new Font("微软雅黑", Font.ITALIC, 45);  //水印字体
        String waterMarkContent = "邱勇琪";   //水印内容
        String tarImgPath = "C:\\Users\\Administrator\\Desktop\\捕获xxx4.PNG";  //原图片

        File file = new File("C:\\Users\\Administrator\\Desktop\\捕获4.PNG");  //存储目标路径
        BufferedImage buImage = ImageIO.read(file);
        int width = buImage.getWidth(); //图片宽
        int height = buImage.getHeight(); //图片高

        //添加水印
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
        Graphics2D g = bufferedImage.createGraphics();
        g.drawImage(buImage, 0, 0, width, height, null);
        g.setColor(color); //水印颜色
        g.setFont(font); //水印字体

        int x = width - 2 * getWatermarkLength(waterMarkContent, g);  //这是一个计算水印位置的函数，可以根据需求添加
        int y = height - 1 * getWatermarkLength(waterMarkContent, g);
                  /*
              g.setAlignment(Image.LEFT | Image.TEXTWRAP);
              g.setBorder(Image.BOX); img.setBorderWidth(10);
              g.setBorderColor(BaseColor.WHITE); img.scaleToFit(100072);//大小
              g.setRotationDegrees(-30);//旋转
             */
        //定义xy
        g.drawString(waterMarkContent, 400, 300); //水印位置

        g.dispose(); //释放资源

        FileOutputStream outImgStream = new FileOutputStream(tarImgPath);
        ImageIO.write(bufferedImage, "jpg", outImgStream);
        System.out.println("添加水印完成");
        outImgStream.flush();
        outImgStream.close();
    }

}




