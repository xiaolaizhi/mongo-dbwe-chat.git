# mongoDBWeChat

#### 介绍
小程序于java后台的交互上传下载分页

#### 软件架构
这是一款小程序上传下载附件分页的一个后端 主要是给想知道小程序前后端是怎么交互的 
小程序入口：[小程序前端入口](http://gitee.com/xiaolaizhi/we-chat-mongo-db.git)

#### 安装教程

1.  先安装mongodb服务器 这个比较简单只要无脑安装就行了  附上安装地址教程 [服务器教程](http://www.runoob.com/mongodb/mongodb-window-install.html)
2.  可以安装客户端 [客户端教程](http://blog.csdn.net/mrjkzhangma/article/details/90082178) 
#### 客户端
![输入图片说明](https://images.gitee.com/uploads/images/2022/0311/104203_e1e7d150_8449960.png "捕获.PNG")

#### 使用说明
1.  不要去创建mongodb的库 直接运行代码就行了没有库的话他会自己创建
####  先附上我的小程序截图
![输入图片说明](https://images.gitee.com/uploads/images/2022/0311/110521_f0fbe61b_8449960.png "捕获2.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0311/110533_0d4f5ac4_8449960.png "捕获4.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2022/0311/110540_30c2af4c_8449960.png "捕获3.PNG")

#### 客户端前端调用接口java 
```
loadimg: function () {
  var _this = this;
  console.log( _this.tempFiles);
  wx.uploadFile({
    url: 'http://localhost:8080/file/uploadFile', //接口
    filePath: _this.data.usernameUrl,
    name: 'file',
    success: function (res) {
      var data = res.data;
      wx.showToast({
        title: '操作成功！', // 标题
        icon: 'success',  // 图标类型，默认success
        duration: 1500  // 提示窗停留时间，默认1500ms
      })
    },
    fail: function (error) {
      wx.showToast({
        title: '操作失败！', // 标题
        icon: 'success',  // 图标类型，默认success
        duration: 1500  // 提示窗停留时间，默认1500ms
      })
      console.log(error);
    }
  })
},

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
